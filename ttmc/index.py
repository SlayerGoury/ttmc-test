import functools

from flask import Blueprint, render_template, request

from ttmc.db import get_db

bp = Blueprint('index', __name__, url_prefix='/')


@bp.route('', methods=('GET', 'POST'))
def index():
	return render_template('index.html')
