DROP TABLE IF EXISTS community;

CREATE TABLE community (
	name VARCHAR(32) UNIQUE NOT NULL,
	enabled BOOLEAN NOT NULL
);

DROP TABLE IF EXISTS snmp;
CREATE TABLE snmp (
	name VARCHAR(32) UNIQUE NOT NULL,
	enabled BOOLEAN NOT NULL,
	passworda VARCHAR(32),
	passwordb VARCHAR(32)
);

DROP TABLE IF EXISTS local;
CREATE TABLE local (
	name VARCHAR(32) UNIQUE NOT NULL,
	enabled BOOLEAN NOT NULL,
	password VARCHAR(32)
);
