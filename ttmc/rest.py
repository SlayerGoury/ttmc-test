import functools

from flask import Blueprint, jsonify, request

from ttmc.db import get_db

bp = Blueprint('rest', __name__, url_prefix='/rest')


class DB:
	def __init__(self):
		self.db = get_db()
		self.allowed_types = ['community', 'snmp', 'local']

	def get_user (self, type, name):
		if type not in self.allowed_types:
			return False
		user = self.db.execute('SELECT * FROM %s WHERE name = ?' % (type, ), (name, )).fetchone()
		return user

	def get_users (self, type):
		if type not in self.allowed_types:
			return False
		users = self.db.execute('SELECT * FROM %s' % (type, ) ).fetchall()
		return users

	def create_user (self, type, name, **kwargs):
		if type == 'community':
			self.db.execute('INSERT INTO community (name, enabled) VALUES (?, ?)', (name, kwargs['enabled'], ))
		elif type == 'snmp':
			enabled = kwargs['enabled'] and kwargs['passworda'] and kwargs['paswordb']
			self.db.execute('INSERT INTO snmp (name, passworda, passwordb, enabled) VALUES (?, ?, ?, ?)', (name, kwargs.get('passworda'), kwargs.get('paswordb'), enabled, ))
		elif type == 'local':
			enabled = kwargs['enabled'] and kwargs['password']
			self.db.execute('INSERT INTO local (name, password, enabled) VALUES (?, ?, ?)', (name, kwargs.get('password'), enabled, ))
		else:
			return False  # TODO raise an error here or better just refactor this part
		self.db.commit()
		return self.get_user(name=name, type=type)

	def update_user (self, type, name, **kwargs):
		if type == 'community':
			self.db.execute('UPDATE community SET enabled=? WHERE name=?', (kwargs['enabled'], name, ))
		elif type == 'snmp':
			enabled = kwargs['enabled'] and kwargs['passworda'] and kwargs['paswordb']
			self.db.execute('UPDATE snmp SET passworda=?, passwordb=?, enabled=? WHERE name=?', (kwargsget('passworda'), kwargsget('paswordb'), enabled, name, ))
		elif type == 'local':
			enabled = kwargs['enabled'] and kwargs['password']
			self.db.execute('UPDATE local SET password=?, enabled=? WHERE name=?', (kwargsget('password'), enabled, name, ))
		else:
			return False  # TODO raise an error here or better just refactor this part
		self.db.commit()
		return self.get_user(name=name, type=type)

	def delete_user (self, type, name):
		if type not in self.allowed_types:
			return False
		self.db.execute('DELETE FROM %s WHERE name = ?' % (type, ), (name, ))
		self.db.commit()
		return True


@bp.route('/community/', methods=('GET', ))
def community_users():
	return jsonify([ dict(user) for user in DB().get_users('community')]), 200


@bp.route('/local/', methods=('GET', ))
def local_users():
	return jsonify([ dict(user) for user in DB().get_users('local')]), 200


@bp.route('/snmp/', methods=('GET', ))
def snmp_users():
	return jsonify([ dict(user) for user in DB().get_users('snmp')]), 200


@bp.route('/community/<string:name>', methods=('GET', 'POST', 'PATCH', 'DELETE', ))
def community_user(name):
	db = DB()
	user = db.get_user(type='community', name=name)
	enabled = True if request.form.get('enabled') == 'true' else False

	if request.method == 'POST':
		if user is not None:
			return jsonify({'message': 'username already taken'}), 409
		user = db.create_user(type='community', name=name, enabled=enabled)
		return jsonify({'message': 'user created', 'user': dict(user)}), 201

	if not user:
		return jsonify({'user': None, 'message': 'no such user'}), 404

	if request.method == 'GET':
		return jsonify({'user': dict(user)}), 200

	if request.method == 'PATCH':
		user = db.update_user(type='community', name=name, enabled=enabled)
		return jsonify({'message': 'user patched', 'user': dict(user)}), 200

	if request.method == 'DELETE':
		db.delete_user(type='community', name=name)
		return jsonify({'message': 'user is no more'}), 200


@bp.route('/local/<string:name>', methods=('GET', 'POST', 'PATCH', 'DELETE', ))
def local_user(name):
	db = DB()
	user = db.get_user(type='local', name=name)
	password = request.form.get('password')
	enabled = True if request.form.get('enabled') == 'true' else False

	if request.method in ['POST', 'PATCH']:
		if not password:
			return jsonify({'message': 'password is required'}), 400

	if request.method == 'POST':
		if user is not None:
			return jsonify({'message': 'username already taken'}), 409
		user = db.create_user(type='local', name=name, password=password, enabled=enabled)
		return jsonify({'message': 'user created', 'user': dict(user)}), 201

	if not user:
		return jsonify({'user': None, 'message': 'no such user'}), 404

	if request.method == 'GET':
		return jsonify({'user': dict(user)}), 200

	if request.method == 'PATCH':
		user = db.update_user(type='local', name=name, password=password, enabled=enabled)
		return jsonify({'message': 'user patched', 'user': dict(user)}), 200

	if request.method == 'DELETE':
		db.delete_user(type='local', name=name)
		return jsonify({'message': 'user is no more'}), 200


@bp.route('/snmp/<string:name>', methods=('GET', 'POST', 'PATCH', 'DELETE', ))
def snmp_user(name):
	db = DB()
	user = db.get_user(type='snmp', name=name)
	passworda = request.form.get('passworda')
	passwordb = request.form.get('passwordb')
	enabled = True if request.form.get('enabled') == 'true' else False

	if request.method in ['POST', 'PATCH']:
		if not passworda and passwordb:
			return jsonify({'message': 'both passwords are required'}), 400

	if request.method == 'POST':
		if user is not None:
			return jsonify({'message': 'username already taken'}), 409
		user = db.create_user(type='snmp', name=name, passworda=passworda, passwordb=passwordb, enabled=enabled)
		return jsonify({'message': 'user created', 'user': dict(user)}), 201

	if not user:
		return jsonify({'user': None, 'message': 'no such user'}), 404

	if request.method == 'GET':
		return jsonify({'user': dict(user)}), 200

	if request.method == 'PATCH':
		user = db.update_user(type='snmp', name=name,  passworda=passworda, passwordb=passwordb, enabled=enabled)
		return jsonify({'message': 'user patched', 'user': dict(user)}), 200

	if request.method == 'DELETE':
		db.delete_user(type='snmp', name=name)
		return jsonify({'message': 'user is no more'}), 200


@bp.route('/user/<string:username>/convert/')
def rest_convert_nocontext(username):
	return jsonify({'message': 'conversion type is required'}), 400


@bp.route('/<string:source>/<string:name>/convert/<string:destination>', methods=('PATCH', ))
def rest_convert(source, name, destination):
	db = DB()
	user = db.get_user(type=source, name=name)

	if source == destination:
		return jsonify({'user': None, 'message': 'nothing to convert here'}), 400

	if not user:
		return jsonify({'user': None, 'message': 'no such user'}), 404

	if db.get_user(type=destination, name=name):
		return jsonify({'message': 'username already taken'}), 409

	if source == 'community':
		user = db.create_user(type=destination, name=name, enabled=False)

	if source == 'snmp':
		if destination != 'local':
			return jsonify({'message': 'this is not allowed'}), 422
		user = db.create_user(type=destination, name=name, password=user['passworda'], enabled=user['enabled'])

	if source == 'local':
		if destination != 'snmp':
			return jsonify({'message': 'this is not allowed'}), 422
		user = db.create_user(type=destination, name=name, passworda=user['password'], passwordb=user['password'], enabled=user['enabled'])

	if user:
		db.delete_user(type=source, name=name)
		return jsonify({'message': 'user converted', 'user': dict(user)}), 200
	else:
		return jsonify({'message': 'database error'}), 500
