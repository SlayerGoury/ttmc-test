function pseudoconsole(text) {
	target = document.getElementById('pseudoconsole');
	target.innerText = text;
}


function submitRequest(what, how) {
	var xmlhttp;
	var url;
	var method;
	var post;
	xmlhttp = new XMLHttpRequest();
	form = document.getElementById(what);
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if ([200, 201].indexOf(xmlhttp.status) != -1) {
				pseudoconsole('ok: '+xmlhttp.responseText);
			} else {
				pseudoconsole('error: '+xmlhttp.responseText);
			}
		}
	}

	if (how == 'convert') {
		name = encodeURIComponent(document.getElementById('form-convert-username').value);
		source = encodeURIComponent(document.getElementById('form-convert-source').value);
		destination = encodeURIComponent(document.getElementById('form-convert-destination').value);
		method = 'PATCH';
		url = `/rest/${source}/${name}/convert/${destination}`
		post = null;
	} else {
		name = encodeURIComponent(document.getElementById('form-name').value);
		passworda = encodeURIComponent(document.getElementById('form-passworda').value);
		passwordb = encodeURIComponent(document.getElementById('form-passwordb').value);
		type = encodeURIComponent(document.getElementById('form-type').value);
		enabled = encodeURIComponent(document.getElementById('form-enabled').checked);
		if (['GET', 'DELETE'].indexOf(how) != -1) { post = null; }
		if (['POST', 'PATCH'].indexOf(how) != -1) {
			if (type == 'community') {post = `enabled=${enabled}`;}
			if (type == 'snmp') {post = `password=${passworda}&passwordb=${passwordb}&enabled=${enabled}`;}
			if (type == 'local') {post = `password=${passworda}&enabled=${enabled}`;}
		}
		method = how;
		url = `/rest/${type}/${name}`;
	}

	xmlhttp.open(method, url, true);
	xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	xmlhttp.send(post);
}
